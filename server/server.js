const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require('body-parser');
const authRoutes = require('./src/ruotes/authRoutes');
const cookieParser = require('cookie-parser');

const PORT = 8080;

let corsOptions = {
    origin: "http://localhost:3000",
    optionSuccessStatus: 200,
    credentials: true,
};

app.get('/', (req, res) => {
    res.send('Hello Prismay');
});

app.listen(PORT, ()=>{
    console.log(`Server listening on ${PORT}`);
});

require('./src/database');

app.use(cors(corsOptions));

app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use(bodyParser.json());
app.use(cookieParser());
app.use('/auth', authRoutes);



