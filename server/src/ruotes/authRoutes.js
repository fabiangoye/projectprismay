const {Router} = require('express');

const authController = require('../controllers/authController');

const router = Router();

//post routes
router.post('/register', authController.postRegister);
router.post('/login', authController.postLogin);
router.post('/logout', authController.postLogout);

module.exports = router;