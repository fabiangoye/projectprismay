const User = require("../models/User");
const jwt = require('jsonwebtoken');

//handling error method

const manejoDeErrores = (error) => {
    let errors = { email: "", password: ""};

    if(error.message === "Login: incorrect password"){
        errors.email = "Email and/or password incorrect";
        errors.password = "Email and/or password incorrect"
    }

    if(error.message === "Login: email address does not exist"){
        errors.email = "Email and/or password incorrect";
        errors.password = "Email and/or password incorrect"
    }

    if(error.code === 11000){
        errors.email = "this email address is already registered";
    }

    if(error.message.includes("User validation failed")){
        Object.values(error.errors).forEach(({properties}) =>{
            errors[properties.path] = properties.message;
        });
    }

    return errors;
};

//method to create the token

const maxAge = 24 * 60 * 60; // 24 hours for the token to expire
const createToken = (id) => {
    return jwt.sign({id}, process.env.JWT_SECRET,{ expiresIn: maxAge});
};

//method to get the token
const getToken = (req) => {
    let token = null;
    let authorization = req.headers.authorization;
    if (authorization != null && authorization != undefined){
        token = authorization.split(" ")[1];
    }
    return token;
};

module.exports.postRegister = async (req, res) => {
    console.log("postRegister");
    // get body username and password
    const {name, lastname, email, password } = req.body;
    console.log(email);
    console.log(password);
    try{
        const user = await User.create({name, lastname, email, password});
        const token = createToken(user._id);
        
    // sending token in the cookie  
        res.cookie("jwt", token, {httpOnly: true, maxAge: maxAge * 1000});
        
        res.status(200).json({ id: user._id, name, email});

    }catch(error){
        const errors = manejoDeErrores(error);
        console.log("wtf")
        res.status(400).json(errors);
    }

};

module.exports.postLogin = async (req, res) => {
    const {email, password} = req.body;
    try{
        const user = await User.login(email, password);

        const token = createToken(user._id);
        res.cookie("jwt", token, {httpOnly: true, maxAge: maxAge * 1000});
        res.status(200).json({ id: user._id, email});
    }catch(error){
        const errors = manejoDeErrores(error);
        res.status(400).json(errors);
    }
};

module.exports.postLogout = async (req, res) => {
    res.cookie("jwt", "", {maxAge : 1});
};
