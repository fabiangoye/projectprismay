const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

const options = {
    useNewUrlParser: true,
    // useFindAndModify: false,
    // useCreateIndex: true,
    useUnifiedTopology: true,
    // autoIndex: false,
    // poolSize: 10,
    // bufferMaxEntries:0
};


const dbConnectionURL = {//mongodb+srv://FabianGoye:<password>@prismay.k7u0x.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
    'URL' : `mongodb+srv://` + process.env.DB_USERNAME +
            `:` + 
            process.env.DB_PASSWORD + 
            `@` +
            process.env.DB_HOST +
            `.k7u0x.mongodb.net/` +
            process.env.DB +
            `?retryWrites=true&w=majority`
};

mongoose.connect(dbConnectionURL.URL, options);

const db = mongoose.connection;
db.on('error', console.log.bind( console, 'Mongodb Connection Error: ' + dbConnectionURL.URL));
db.once('open', () => {
    console.log('Mongodb Connection Successful');
});



