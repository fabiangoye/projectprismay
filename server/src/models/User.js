const mongoose = require('mongoose');
const {isEmail} = require('validator');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'type your name here']
    },
    lastname: {
        type: String,
        required: [true, 'type your lastname here']
    },
    email: {
        type: String,
        required: [true,'type  your email here'],
        unique: [true, 'this email is already registered'],
        index: true,
        lowercase: true,
        validate: [isEmail, 'the email address is not valid']

    },
    password : {// visualizarla con el botón del ojo
        type: String,
        required: [true, 'type your password here'],
        minlength:[6, 'the password must have a minimun length of 6']
    }


});

/**** encriptación previa de la contraseña *****/
userSchema.pre('save', async function(next){
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next(); 
});

/**** login method****/
userSchema.statics.login = async function(email, password){
    const user = await User.findOne({email: email});
    if(user){
        const autorizado = await bcrypt.compare(password, user.password);
        if (autorizado){
            return user._id;
        }else{
            throw Error('Login: incorrect password');
        }
    }else{
        throw Error('Login: email address does not exist')
    }
};

const User = mongoose.model('User', userSchema);
module.exports = User;

